package manoty.utils;

import java.util.Random;

public class MakeColor {

    public static String rndColor() {
        Random random = new Random();
        int num = random.nextInt(16777215);
        String hex = "";
        while (num != 0) {
            if (num % 16 < 10)
                hex = Integer.toString(num % 16) + hex;
            else
                hex = (char) ((num % 16) + 55) + hex;
            num = num / 16;
        }

        return ((hex.length() < 6) ? String.format("%0" + (6 - hex.length()) + "d", 0) : "") + hex;
    }
}
