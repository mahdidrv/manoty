package manoty.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "note")
public class Note implements Serializable {
    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "desc")
    private String description;
    @ColumnInfo(name = "time")
    private String time;
    @ColumnInfo(name = "color")
    private String color;

    public Note(int id, String title, String description, String time, String color) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.time = time;
        this.color = color;
    }

    @Ignore
    public Note(String title, String description, String time, String color) {
        this.title = title;
        this.description = description;
        this.time = time;
        this.color = color;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getTime() {
        return time;
    }

    public String getColor() {
        return color;
    }
}
