package manoty.database;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface NoteDao {

    @Query("select * from Note")
    Single<List<Note>> getAllNotes();

    @Query("UPDATE note SET title= :title, `desc`= :decs  WHERE id= :position")
    void editItem (String title, String decs, int position);

    @Update
    void editItem(Note note);

    @Delete
    void deleteNote(Note note);

    @Insert
    void insertNote(Note note);

}
