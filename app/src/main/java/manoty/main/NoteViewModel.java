package manoty.main;

import android.content.Context;

import java.util.List;

import io.reactivex.Single;
import manoty.database.Note;
import manoty.database.NoteDatabase;


public class NoteViewModel {

    private NoteDatabase noteDatabase;
//    public RoomWithRxJavaViewModel(@NonNull Application application)     {
//        super(application);
//        appDatabase = MyDataBase.getDatabase(application);
//    }


    public NoteViewModel (Context context){
        noteDatabase = NoteDatabase.getInstance(context);
    }


    Single<List<Note>> getList() {
        return noteDatabase.noteDao().getAllNotes();
    }
}
