package manoty.main;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import ir.mahdidrv.manoty.R;
import manoty.editor.NoteEditor;
import manoty.utils.DateMaker;
import manoty.utils.MakeColor;
import manoty.database.Note;
import manoty.database.NoteDatabase;


// create by Mahdi Darvishi in bahman 1398
//    Contact me with Instagram => @Mahdi.drv

public class MainActivity extends AppCompatActivity implements NoteAdapter.onItemListener {

    private static final String TAG = "main";
    public List<Note> notes;
    private NoteViewModel viewModel;
    private NoteDatabase noteDatabase;
    private RecyclerView rvNotes;
    private NoteAdapter noteAdapter;
    public static String INTENT_KEY_TITLE = "note_title";
    public static String INTENT_KEY_DESC = "note_desc";
    public static String INTENT_KEY_MODE = "note_mode";
    public static String INTENT_KEY_NOTE = "note_note";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // initialize adapter, database and etc
        noteDatabase = NoteDatabase.getInstance(this);
        viewModel = new NoteViewModel(this);
        notes = new ArrayList<>();
        noteAdapter = new NoteAdapter();

        //insertData();
        setupView();
        observ();
    }

    private void observ() {


        // send request with rx to get my all notes from database
        viewModel.getList()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<List<Note>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<Note> notes) {
                        noteAdapter.setNotes(notes, MainActivity.this);
                        rvNotes.setAdapter(noteAdapter);
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                });


    }

    private void setupView() {

        rvNotes = findViewById(R.id.rv_main_notes);
        rvNotes.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, true));


        ImageView addNoteBtn = findViewById(R.id.iv_main_addNote);
        addNoteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, NoteEditor.class);
                intent.putExtra(MainActivity.INTENT_KEY_MODE, false);
                startActivityForResult(intent, 20);
            }
        });

    }


    // fake data for test
    private void insertData() {

        for (int i = 0; i < 20; i++) {
            noteDatabase.noteDao().insertNote(new Note(
                    "تایتل نمایشی" + i,
                    "من یک دیسکریپشن نمایشی هستم !",
                    DateMaker.getCurrentShamsidate(), MakeColor.rndColor()));
        }
    }


    // get data from NoteEditor and check i change my note or take a new note
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 20 && resultCode == Activity.RESULT_OK) {

            Log.i(TAG, "onActivityResult: ");
            Boolean isEditMode = data.getBooleanExtra(INTENT_KEY_MODE, true);
            Log.i(TAG, "onActivityResult: " + isEditMode);

            if (isEditMode) {
                Note note = (Note) data.getSerializableExtra(INTENT_KEY_NOTE);
                if (note != null) {
                    //noteDatabase.noteDao().editItem(note.getTitle(), note.getDescription(), note.getId());
                    noteDatabase.noteDao().editItem(note);
                    observ();
                    //noteAdapter.updateNote(note, note.getId());
                    Log.i(TAG, "onActivityResult: ");
                }
            }

            if (!isEditMode) {
                String title = data.getStringExtra(INTENT_KEY_TITLE);
                String desc = data.getStringExtra(INTENT_KEY_DESC);

                Log.i(TAG, "onActivityResult: ");
                Log.i(TAG, "onActivityResult: " + title + " " + desc + " " + DateMaker.getCurrentShamsidate() + " " + MakeColor.rndColor());
                Note newNote = new Note(title, desc, DateMaker.getCurrentShamsidate(), MakeColor.rndColor());
                noteDatabase.noteDao().insertNote(newNote);
                noteAdapter.addNote(newNote);
            }

        }
        Log.i(TAG, "onActivityResult: " + " is null?");

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setOnItemClickListener(Note note) {
        Intent intent = new Intent(this, NoteEditor.class);
        intent.putExtra(MainActivity.INTENT_KEY_NOTE, note);
        intent.putExtra(MainActivity.INTENT_KEY_MODE, true);

        startActivityForResult(intent, 20);
    }

    @Override
    public void setOnItemHoverListener(Note note, int position) {
        noteDatabase.noteDao().deleteNote(note);
        noteAdapter.deleteNote(position);
        observ();
    }
}
