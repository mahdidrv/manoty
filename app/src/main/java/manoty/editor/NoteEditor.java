package manoty.editor;


import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ir.mahdidrv.manoty.R;
import manoty.database.Note;
import manoty.main.MainActivity;

public class NoteEditor extends AppCompatActivity {


    EditText titleEt;
    EditText descEt;
    Button saveBtn;
    Note note;
    boolean isEditMode;
    Button nighModeBtn;
    boolean isNightMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_editor);


        //night mode
/*
        nighModeBtn = findViewById(R.id.btn_editor_nightMode);
        nighModeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isNightMode) {


                    setNightMode(R.drawable.bg_edittext_night,"#FFFFFF");
                    isNightMode = true;

                } else {

                    setNightMode(R.drawable.bg_edittext,"#444444");
                    isNightMode = false;

                }
            }
        });
*/


        titleEt = findViewById(R.id.edt_editor_title);
        descEt = findViewById(R.id.edt_editor_decs);
        saveBtn = findViewById(R.id.btn_editor_save);


        note = (Note) getIntent().getSerializableExtra(MainActivity.INTENT_KEY_NOTE);
        isEditMode = getIntent().getBooleanExtra(MainActivity.INTENT_KEY_MODE, true);

        if (isEditMode) {
            titleEt.setText(note.getTitle());
            descEt.setText(note.getDescription());
        }

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent returnIntent = new Intent();
                returnIntent.putExtra(MainActivity.INTENT_KEY_MODE, isEditMode);

                if (isEditMode) {
                    note.setTitle(titleEt.getText().toString());
                    note.setDescription(descEt.getText().toString());
                    returnIntent.putExtra(MainActivity.INTENT_KEY_NOTE, note);

                } else {
                    returnIntent.putExtra(MainActivity.INTENT_KEY_TITLE, titleEt.getText().toString());
                    returnIntent.putExtra(MainActivity.INTENT_KEY_DESC, descEt.getText().toString());
                }

                setResult(Activity.RESULT_OK, returnIntent);
                finish();

            }
        });


    }

    //night mode for editor
/*
    private void setNightMode(int drawable, String textColor) {

        int textColorHex = Color.parseColor(textColor);
        Drawable bgDrawbale = ContextCompat.getDrawable(NoteEditor.this,drawable);

        titleEt.setBackground(bgDrawbale);
        descEt.setBackground(bgDrawbale);

        titleEt.setTextColor(textColorHex);
        descEt.setTextColor(textColorHex);

    }*/
}
