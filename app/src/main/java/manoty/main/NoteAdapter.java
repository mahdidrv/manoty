package manoty.main;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ir.mahdidrv.manoty.R;
import manoty.database.Note;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {

    private List<Note> notes;
    private onItemListener onSave;

    public void deleteNote(int position){
        notes.remove(position);
        notifyItemRemoved(position);
    }

    public void updateNote(Note note, int position){
        notes.set(position,note);
        notifyItemChanged(position);
    }

    public void addNote(Note note){
        notes.add(notes.size(),note);
        notifyItemInserted(notes.size());
    }

    // setter like costructor / set interface
    public void setNotes(List<Note> notes, onItemListener onSave )
    {
        this.notes = notes;
        this.onSave = onSave;
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NoteViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.note_item,parent,false));
    }


    @Override
    public void onBindViewHolder(@NonNull final NoteViewHolder holder, final int position) {
        holder.itemNote(notes.get(position));

        // and save button for save or edit like hover i use interface
        holder.cvBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  onSave.setOnItemClickListener(notes.get(position));
            }
        });

        //Delete note: i make a interface when click (hover) on item, tell MainActivity to delete my note in database and adapter
        holder.cvBox.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.i("main", "onHover: ");
                onSave.setOnItemHoverListener(notes.get(position),position);
                return false;
            }
        });


    }


    @Override
    public int getItemCount() {
        return notes.size();
    }

    public class NoteViewHolder extends RecyclerView.ViewHolder{

        TextView title;
        TextView desc;
        TextView date;
        View line;
        CardView cvBox;


        public NoteViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.txt_itemNote_title);
            desc = itemView.findViewById(R.id.txt_itemNote_desc);
            date = itemView.findViewById(R.id.txt_noteItem_date);
            line = itemView.findViewById(R.id.view_noteItem_line);
            cvBox = itemView.findViewById(R.id.cv_noteItem_box);

        }

        public void itemNote (Note note){
            title.setText(note.getTitle());
            desc.setText(note.getDescription());
            date.setText(note.getTime());

            //change text and shape background color with hex code
            String colorCode = "#"+note.getColor();
            title.setTextColor(Color.parseColor(colorCode));
            line.setBackgroundColor(Color.parseColor(colorCode));
        }
    }

    // interface
    public interface onItemListener{
        void setOnItemClickListener(Note note);
        void setOnItemHoverListener(Note note, int position);
    }
}
